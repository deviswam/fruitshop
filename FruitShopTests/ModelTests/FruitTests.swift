//
//  FruitTests.swift
//  FruitShopTests
//
//  Created by Waheed Malik on 02/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FruitShop

class FruitTests: XCTestCase {
    func testAnAppleCostsSixtyPence() {
        // Given
        let sut = Fruit.apple
        // Then
        XCTAssert(sut.price == APPLE_COST)
    }

    func testAnOrangeCostsTwentyFivePence() {
        // Given
        let sut = Fruit.orange
        // Then
        XCTAssert(sut.price == ORANGE_COST)
    }
}
