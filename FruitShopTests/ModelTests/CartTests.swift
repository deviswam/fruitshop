//
//  CartTests.swift
//  FruitShopTests
//
//  Created by Waheed Malik on 02/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FruitShop

class CartTests: XCTestCase {
    
    func testCartIsEmpty() {
        // Given
        let sut = Cart(fruits:[])
        // Then
        XCTAssert(sut.fruits.count == 0)
    }
    
    func testCartHasOneFruit() {
        // Given
        let sut = Cart(fruits:[.apple])
        // Then
        XCTAssert(sut.fruits.count == 1)
    }
    
    func testCartHasTwoFruits() {
        // Given
        let sut = Cart(fruits:[.apple, .orange])
        // Then
        XCTAssert(sut.fruits.count == 2)
    }
    
    func testSubTotal_whenCartHasOneApple_returnsSixtyPence() {
        // Given
        let sut = Cart(fruits:[.apple])
        // Then
        XCTAssert(sut.subTotal == APPLE_COST)
    }
    
    func testSubTotal_whenCartHasOneOrange_returnsTwentyFivePence() {
        // Given
        let sut = Cart(fruits:[.orange])
        // Then
        XCTAssert(sut.subTotal == ORANGE_COST)
    }
    
    func testSubTotal_whenCartHasTwoApples_returnsOnePoundTwentyPence() {
        // Given
        let sut = Cart(fruits:[.apple, .apple])
        let expectedTotal = APPLE_COST.multiplying(by: 2)
        // Then
        XCTAssert(sut.subTotal == expectedTotal)
        XCTAssert(sut.subTotal == 1.20)
    }
    
    func testSubTotal_whenCartHasTwoOranges_returnsFiftyPence() {
        // Given
        let sut = Cart(fruits:[.orange, .orange])
        let expectedTotal = ORANGE_COST.multiplying(by: 2)
        // Then
        XCTAssert(sut.subTotal == expectedTotal)
        XCTAssert(sut.subTotal == 0.50)
    }
    
    func testSubTotal_whenCartHasOneAppleAndOneOrange_returnsEightyFivePence() {
        // Given
        let sut = Cart(fruits:[.apple, .orange])
        let expectedTotal = APPLE_COST.adding(ORANGE_COST)
        // Then
        XCTAssert(sut.subTotal == expectedTotal)
        XCTAssert(sut.subTotal == 0.85)
    }
    
    func testSubTotal_whenCartHasTwoApplesAndThreeOranges_returnsOnePoundNinetyFivePence() {
        // Given
        let sut = Cart(fruits:[.apple, .orange, .apple, .orange, .orange])
        let expectedTotal = APPLE_COST.multiplying(by: 2).adding(ORANGE_COST.multiplying(by: 3))
        // Then
        XCTAssert(sut.subTotal == expectedTotal)
        XCTAssert(sut.subTotal == 1.95)
    }
    
    func testFruitCount_whenCartHasOneApple_shouldReturnOne() {
        // Given
        let sut = Cart(fruits:[.apple])
        // When
        let appleCount = sut.fruitCount(ofType: .apple)
        // Then
        XCTAssert(appleCount == 1)
    }
    
    func testFruitCount_whenCartHasTwoApples_shouldReturnTwo() {
        // Given
        let sut = Cart(fruits:[.apple, .apple])
        // When
        let appleCount = sut.fruitCount(ofType: .apple)
        // Then
        XCTAssert(appleCount == 2)
    }
    
    func testFruitCount_forApples_whenCartHasTwoApplesAndOneOrange_shouldReturnTwo() {
        // Given
        let sut = Cart(fruits:[.apple, .apple, .orange])
        // When
        let appleCount = sut.fruitCount(ofType: .apple)
        // Then
        XCTAssert(appleCount == 2)
    }
    
    func testFruitCount_whenCartHasOneOrange_shouldReturnOne() {
        // Given
        let sut = Cart(fruits:[.orange])
        // When
        let orangeCount = sut.fruitCount(ofType: .orange)
        // Then
        XCTAssert(orangeCount == 1)
    }
    
    func testFruitCount_whenCartHasTwoOranges_shouldReturnTwo() {
        // Given
        let sut = Cart(fruits:[.orange, .orange])
        // When
        let orangeCount = sut.fruitCount(ofType: .orange)
        // Then
        XCTAssert(orangeCount == 2)
    }
    
    func testFruitCount_forOranges_whenCartHasTwoOrangesAndOneApple_shouldReturnTwo() {
        // Given
        let sut = Cart(fruits:[.orange, .orange, .apple])
        // When
        let orangeCount = sut.fruitCount(ofType: .orange)
        // Then
        XCTAssert(orangeCount == 2)
    }
}
