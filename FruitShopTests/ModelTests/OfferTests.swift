//
//  OfferTests.swift
//  FruitShopTests
//
//  Created by Waheed Malik on 03/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FruitShop

class OfferTests: XCTestCase {
    
    var threeForTwoOffer: Offer!
    var buyOneGetOneFreeOffer: Offer!
    
    override func setUp() {
        super.setUp()
        threeForTwoOffer = Offer(of: .threeForTwo, on: .orange)
        buyOneGetOneFreeOffer = Offer(of: .buyOneGetOneFree, on: .apple)
    }
    
    override func tearDown() {
        threeForTwoOffer = nil
        buyOneGetOneFreeOffer = nil
        super.tearDown()
    }
    
    func testOfferIsAppliedOnFruitType() {
        // Then
        XCTAssert(threeForTwoOffer.fruit == .orange)
    }
    
    func testDiscount_withZeroQuantity_returnsZero() {
        // When
        let discount = threeForTwoOffer.discount(quantity: 0)
        // Then
        XCTAssert(discount == 0)
    }
    
    
    // MARK:- *** THREE FOR TWO OFFER TESTS ***
    
    
    func testDiscount_onThreeForTwoOffer_withOneOrange_returnsZero() {
        // When
        let discount = threeForTwoOffer.discount(quantity: 1)
        // Then
        XCTAssert(discount == 0)
    }
    
    func testDiscount_onThreeForTwoOffer_withTwoOranges_returnsZero() {
        // When
        let discount = threeForTwoOffer.discount(quantity: 2)
        // Then
        XCTAssert(discount == 0)
    }
    
    func testDiscount_onThreeForTwoOffer_withThreeOranges_returnsTwentyFivePence() {
        // When
        let discount = threeForTwoOffer.discount(quantity: 3)
        // Then
        XCTAssert(discount == 0.25)
    }
    
    func testDiscount_onThreeForTwoOffer_withFiveOranges_returnsTwentyFivePence() {
        // When
        let discount = threeForTwoOffer.discount(quantity: 5)
        // Then
        XCTAssert(discount == 0.25)
    }
    
    func testDiscount_onThreeForTwoOffer_withThirteenOranges_returnsOnePound() {
        // When
        let discount = threeForTwoOffer.discount(quantity: 13)
        // Then
        XCTAssert(discount == 1.00)
    }
    
    func testDiscount_onThreeForTwoOffer_withFourteenOranges_returnsOnePound() {
        // When
        let discount = threeForTwoOffer.discount(quantity: 14)
        // Then
        XCTAssert(discount == 1.00)
    }
    
    func testDiscount_onThreeForTwoOffer_withTwentyThreeOranges_returnsOnePoundSeventyFivePence() {
        // When
        let discount = threeForTwoOffer.discount(quantity: 23)
        // Then
        XCTAssert(discount == 1.75)
    }
    
    
    // MARK:- *** BUY ONE GET ONE FREE OFFER TESTS ***
    
    
    func testDiscount_onBuyOneGetOneFreeOffer_withOneApple_returnsZero() {
        // When
        let discount = buyOneGetOneFreeOffer.discount(quantity: 1)
        // Then
        XCTAssert(discount == 0)
    }
    
    func testDiscount_onBuyOneGetOneFreeOffer_withTwoApples_returnsSixtyPence() {
        // When
        let discount = buyOneGetOneFreeOffer.discount(quantity: 2)
        // Then
        XCTAssert(discount == 0.60)
    }
    
    func testDiscount_onBuyOneGetOneFreeOffer_withThreeApples_returnsSixtyPence() {
        // When
        let discount = buyOneGetOneFreeOffer.discount(quantity: 3)
        // Then
        XCTAssert(discount == 0.60)
    }
    
    func testDiscount_onBuyOneGetOneFreeOffer_withFiveApples_returnsOnePoundsTwentyPence() {
        // When
        let discount = buyOneGetOneFreeOffer.discount(quantity: 5)
        // Then
        XCTAssert(discount == 1.20)
    }
    
    func testDiscount_onBuyOneGetOneFreeOffer_withThirteenApples_returnsThreePoundsSixtyPence() {
        // When
        let discount = buyOneGetOneFreeOffer.discount(quantity: 13)
        // Then
        XCTAssert(discount == 3.60)
    }
    
    func testDiscount_onBuyOneGetOneFreeOffer_withFourteenApples_returnsFourPoundsTwentyPence() {
        // When
        let discount = buyOneGetOneFreeOffer.discount(quantity: 14)
        // Then
        XCTAssert(discount == 4.20)
    }
    
    func testDiscount_onBuyOneGetOneFreeOffer_withTwentyThreeApples_returnsSixPoundsSixtyPence() {
        // When
        let discount = buyOneGetOneFreeOffer.discount(quantity: 23)
        // Then
        XCTAssert(discount == 6.60)
    }
}
