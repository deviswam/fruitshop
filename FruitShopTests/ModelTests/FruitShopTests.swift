//
//  FruitShopTests.swift
//  FruitShopTests
//
//  Created by Waheed Malik on 02/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FruitShop

class FruitShopTests: XCTestCase {
    
    var sut: FruitShop!
    
    override func setUp() {
        super.setUp()
        sut = FruitShop()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testCheckout_withEmptyCart_returnsZeroAsTotal() {
        // When
        let subTotal = sut.checkout(cart: Cart(fruits: []), offers: [])
        // Then
        XCTAssert(subTotal == 0)
    }
    
    func testCheckout_withThreeOrangesInCartAndNoOffers_returnsSeventyFivePenceAsTotal() {
        let cart = Cart(fruits: [.orange, .orange, .orange])
        let offers = [Offer]()
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 0.75)
    }
    
    func testCheckout_withThreeOrangesInCartAndThreeForTwoOffer_returnsFiftyPenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.orange, .orange, .orange])
        let offers = [Offer(of: .threeForTwo, on: .orange)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 0.50)
    }
    
    func testCheckout_withFiveOrangesInCartAndThreeForTwoOffer_returnsOnePoundAsTotal() {
        // Given
        let cart = Cart(fruits: [.orange, .orange, .orange, .orange, .orange])
        let offers = [Offer(of: .threeForTwo, on: .orange)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 1.00)
    }
    
    func testCheckout_withThirteenOrangesInCartAndThreeForTwoOffer_returnsTwoPoundsAndTwentyFivePenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.orange, .orange, .orange, .orange, .orange,
                                 .orange, .orange, .orange, .orange, .orange,
                                 .orange, .orange, .orange])
        let offers = [Offer(of: .threeForTwo, on: .orange)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 2.25)
    }
    
    func testCheckout_withThreeOrangesAndTwoApplesInCart_andThreeForTwoOfferOnOranges_returnsOnePoundSeventyPenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.orange, .apple, .orange, .apple, .orange])
        let offers = [Offer(of: .threeForTwo, on: .orange)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 1.70)
    }
    
    func testCheckout_withFiveOrangesAndTwoApplesInCart_andThreeForTwoOfferOnOranges_returnsTwoPoundsAndTwentyPenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.orange, .orange, .orange, .orange, .orange, .apple, .apple])
        let offers = [Offer(of: .threeForTwo, on: .orange)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 2.20)
    }
    
    func testCheckout_withTwoApplesInCartAndNoOffers_returnsOnePoundTwentyPenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.apple, .apple])
        let offers = [Offer]()
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 1.20)
    }
    
    func testCheckout_withTwoApplesInCartAndBuyOneGetOneFreeOffer_returnsSixtyPenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.apple, .apple])
        let offers = [Offer(of: .buyOneGetOneFree, on: .apple)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 0.60)
    }
    
    func testCheckout_withFiveApplesInCartAndBuyOneGetOneFreeOffer_returnsOnePoundAndEightyPenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.apple, .apple, .apple, .apple, .apple])
        let offers = [Offer(of: .buyOneGetOneFree, on: .apple)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 1.80)
    }
    
    func testCheckout_withThirteenApplesInCartAndBuyOneGetOneFreeOffer_returnsFourPoundsAndTwentyPenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.apple, .apple, .apple, .apple, .apple,
                                 .apple, .apple, .apple, .apple, .apple,
                                 .apple, .apple, .apple])
        let offers = [Offer(of: .buyOneGetOneFree, on: .apple)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 4.20)
    }
    
    func testCheckout_withThreeApplesAndTwoOrangesInCart_andBuyOneGetOneFreeOfferOnApples_returnsOnePoundSeventyPenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.apple, .orange, .apple, .orange, .apple])
        let offers = [Offer(of: .buyOneGetOneFree, on: .apple)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 1.70)
    }
    
    func testCheckout_withFiveApplesAndTwoOrangesInCart_andBuyOneGetOneOfferOnApples_returnsTwoPoundsAndThirtyPenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.apple, .apple, .orange, .apple, .apple, .apple, .orange])
        let offers = [Offer(of: .buyOneGetOneFree, on: .apple)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 2.30)
    }
    
    func testCheckout_withFiveApplesAndFiveOrangesInCart_withRespectiveOffers_returnsTwoPoundsAndEightyPenceAsTotal() {
        // Given
        let cart = Cart(fruits: [.apple, .apple, .orange, .apple, .orange, .apple, .apple, .orange, .orange, .orange])
        let offers = [Offer(of: .threeForTwo, on: .orange), Offer(of: .buyOneGetOneFree, on: .apple)]
        // When
        let subTotal = sut.checkout(cart: cart, offers: offers)
        // Then
        XCTAssert(subTotal == 2.80)
    }
}
