//
//  CheckoutViewModelTests.swift
//  FruitShopTests
//
//  Created by Waheed Malik on 03/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FruitShop

class CheckoutViewModelTests: XCTestCase {
    var sut: CheckoutViewModelImpl!
    
    override func setUp() {
        super.setUp()
        sut = CheckoutViewModelImpl(fruitShop: FruitShop())
    }
    
    func testConformanceToCheckoutViewModelProtocol() {
        XCTAssert((sut as AnyObject) is CheckoutViewModel)
    }
    
    func testViewModelHasFruitShop() {
        XCTAssertNotNil(sut.fruitShop)
    }
    
    func testCheckout_withZeroFruits_returnsZeroPounds() {
        // When
        let total = try! sut.checkout(noOfApples: "0", noOfOranges: "0")
        // Then
        XCTAssert(total == "The total is £0")
    }
    
    func testCheckout_withThreeOrangesAndNoOffers_returnsSeventyFivePenceAsString() {
        // When
        let total = try! sut.checkout(noOfApples: "0", noOfOranges: "3")
        // Then
        XCTAssert(total == "The total is £0.75")
    }
    
    func testCheckout_withThreeOrangesAndThreeForTwoOffer_returnsFiftyPenceAsString() {
        // When
        let threeForTwoOffer = OfferType.threeForTwo.rawValue
        let total = try! sut.checkout(noOfApples: "0", noOfOranges: "3", orangeOffer:threeForTwoOffer)
        // Then
        XCTAssert(total == "The total is £0.5")
    }
    
    func testCheckout_withTwoApplesAndBuyOneGetOneFreeOffer_returnsSixtyPenceAsString() {
        // When
        let buyOneGetOneFree = OfferType.buyOneGetOneFree.rawValue
        let total = try! sut.checkout(noOfApples: "2", noOfOranges: "0", appleOffer: buyOneGetOneFree)
        // Then
        XCTAssert(total == "The total is £0.6")
    }
    
    func testCheckout_withFiveApplesAndFiveOranges_withRespectiveOffers_returnsTwoPoundsAndEightyPenceAsString() {
        // When
        let threeForTwoOffer = OfferType.threeForTwo.rawValue
        let buyOneGetOneFree = OfferType.buyOneGetOneFree.rawValue
        let total = try! sut.checkout(noOfApples: "5", noOfOranges: "5", appleOffer: buyOneGetOneFree, orangeOffer: threeForTwoOffer)
        // Then
        XCTAssert(total == "The total is £2.8")
    }
}

