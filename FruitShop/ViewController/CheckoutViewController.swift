//
//  ViewController.swift
//  FruitShop
//
//  Created by Waheed Malik on 03/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class CheckoutViewController: UIViewController {
    
    var viewModel: CheckoutViewModel?
    
    @IBOutlet weak var appleQuantityField: UITextField!
    @IBOutlet weak var appleRateLabel: UILabel!
    @IBOutlet weak var appleOfferSegment: UISegmentedControl!
    
    @IBOutlet weak var orangeQuantityField: UITextField!
    @IBOutlet weak var orangeRateLabel: UILabel!
    @IBOutlet weak var orangeOfferSegment: UISegmentedControl!
    
    @IBOutlet weak var totalLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appleRateLabel.text = viewModel?.price(of: .apple)
        orangeRateLabel.text = viewModel?.price(of: .orange)
    }
    
    @IBAction func checkoutButtonPressed(_ sender: Any) {
        guard let noOfApples = appleQuantityField.text, !noOfApples.isEmpty,
            let noOfOranges = orangeQuantityField.text, !noOfOranges.isEmpty else {
                showAlert(withErrorMessage: CheckoutViewModelError.invalidInputData.localizedDescription)
                return
        }
        
        let appleOfferIndex = appleOfferSegment.selectedSegmentIndex != 0 ?
            appleOfferSegment.selectedSegmentIndex : nil
        let orangeOfferIndex = orangeOfferSegment.selectedSegmentIndex != 0 ?
            orangeOfferSegment.selectedSegmentIndex : nil
        
        do {
            let checkoutTotalStr = try viewModel?.checkout(noOfApples: noOfApples, noOfOranges: noOfOranges,
                                                           appleOffer: appleOfferIndex, orangeOffer: orangeOfferIndex)
            totalLabel.text = checkoutTotalStr
        } catch(let error) {
            showAlert(withErrorMessage: error.localizedDescription)
        }
    }
}

extension CheckoutViewController {
    private func showAlert(withErrorMessage message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
