//
//  FruitShop.swift
//  FruitShop
//
//  Created by Waheed Malik on 02/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

struct FruitShop {
    func checkout(cart: Cart, offers: [Offer]) -> NSDecimalNumber {
        var cartTotal = cart.subTotal
        offers.forEach { offer in
            let fruitCount = cart.fruitCount(ofType: offer.fruit)
            let offerDiscount = offer.discount(quantity: fruitCount)
            cartTotal = cartTotal.subtracting(offerDiscount)
        }
        return cartTotal
    }
}
