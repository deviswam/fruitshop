//
//  Cart.swift
//  FruitShop
//
//  Created by Waheed Malik on 02/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

struct Cart {
    let fruits: [Fruit]
    
    var subTotal: NSDecimalNumber {
        return fruits.reduce(0.0) { (result: NSDecimalNumber, fruit) -> NSDecimalNumber  in
            return fruit.price.adding(result)
        }
    }
    
    func fruitCount(ofType type: Fruit) -> Int {
        return fruits.filter({ fruit -> Bool in
            return fruit == type
        }).count
    }
}
