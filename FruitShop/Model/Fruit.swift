//
//  Fruit.swift
//  FruitShop
//
//  Created by Waheed Malik on 02/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

let APPLE_COST = NSDecimalNumber(value: 0.60)
let ORANGE_COST = NSDecimalNumber(value: 0.25)

enum Fruit {
    case apple
    case orange
    
    var price: NSDecimalNumber {
        switch self {
        case .apple:
            return APPLE_COST
        case .orange:
            return ORANGE_COST
        }
    }
}
