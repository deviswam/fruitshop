//
//  Offer.swift
//  FruitShop
//
//  Created by Waheed Malik on 03/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

enum OfferType: Int {
    case threeForTwo = 1
    case buyOneGetOneFree = 2
}

struct Offer {
    private let THREE_FOR_TWO_OFFER_DIVISOR = 3
    private let BUY_ONE_GET_ONE_FREE_OFFER_DIVISOR = 2
    
    private let divisor: Int
    
    let fruit: Fruit
    
    init(of type: OfferType, on fruit: Fruit) {
        self.fruit = fruit
        switch type {
        case .threeForTwo:
            divisor = THREE_FOR_TWO_OFFER_DIVISOR
        case .buyOneGetOneFree:
            divisor = BUY_ONE_GET_ONE_FREE_OFFER_DIVISOR
        }
    }
    
    func discount(quantity: Int) -> NSDecimalNumber {
        guard quantity >= divisor else {
            return 0
        }
        
        let noOfOffers = quantity / divisor
        let discountAmount = fruit.price.multiplying(by: NSDecimalNumber(value: noOfOffers))
        return discountAmount
    }
}

