//
//  AppDelegate.swift
//  FruitShop
//
//  Created by Waheed Malik on 02/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let checkoutVC = (window?.rootViewController as! UINavigationController).topViewController as! CheckoutViewController
        checkoutVC.viewModel = CheckoutViewModelImpl(fruitShop: FruitShop())
        return true
    }
}

