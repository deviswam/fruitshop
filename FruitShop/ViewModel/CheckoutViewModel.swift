//
//  CheckoutViewModel.swift
//  FruitShop
//
//  Created by Waheed Malik on 03/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

enum CheckoutViewModelError: Error {
    case invalidInputData
    
    var localizedDescription: String {
        switch self {
        case .invalidInputData:
            return "Either a field is missing or input data is not valid"
        }
    }
}

protocol CheckoutViewModel {
    func checkout(noOfApples: String, noOfOranges: String, appleOffer: Int?, orangeOffer: Int?) throws -> String
    func price(of fruit: Fruit) -> String
}

class CheckoutViewModelImpl: CheckoutViewModel {
    let fruitShop: FruitShop
    init(fruitShop: FruitShop) {
        self.fruitShop = fruitShop
    }
    
    func price(of fruit: Fruit) -> String {
        return "@ " + String(describing: fruit.price) + "p / item"
    }
    
    func checkout(noOfApples: String, noOfOranges: String, appleOffer: Int? = nil, orangeOffer: Int? = nil) throws -> String {
        guard let noOfApples = Int(noOfApples),
            let noOfOranges = Int(noOfOranges) else {
                throw CheckoutViewModelError.invalidInputData
        }
        
        var fruits = [Fruit]()
        
        let apples = Array(repeating: Fruit.apple, count: noOfApples)
        let oranges = Array(repeating: Fruit.orange, count: noOfOranges)
        fruits = apples + oranges
        let cart = Cart(fruits: fruits)
        let offers = createOffers(appleOffer: appleOffer, orangeOffer: orangeOffer)
        let checkoutTotal = fruitShop.checkout(cart: cart, offers: offers)
        
        return "The total is £" + String(describing: checkoutTotal)
    }
    
    //MARK: Private Helper methods
    
    private func createOffers(appleOffer: Int?, orangeOffer: Int?) -> [Offer] {
        var offers = [Offer]()
        if let appleOffer = createOffer(from: appleOffer, fruit: .apple) {
            offers.append(appleOffer)
        }
        if let orangeOffer = createOffer(from: orangeOffer, fruit: .orange) {
            offers.append(orangeOffer)
        }
        return offers
    }
    
    private func createOffer(from offerIndex: Int?, fruit: Fruit) -> Offer? {
        guard let offerIndex = offerIndex,
              let offerType = OfferType(rawValue: offerIndex) else {
            return nil
        }
        return Offer(of: offerType, on: fruit)
    }
}
