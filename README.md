IMPORTANT NOTES
===============

1. The app is following `MVVM (Model-View-ViewModel)` architecture pattern.
2. The project fully implements the fruit shop's checkout system with both `shopping cart` and `simple offers` steps. Two extra (bouns) steps are also implemented which target application `View` and `ViewModel`. All steps are clearly marked as seperate git commits.
3. Its a universal app and supports all major iPhone and iPad screens  
4. The project has been developed on latest `XCode 9.3` with `iOS 11.3` and `Swift 4.1` without any warnings or errors.
5. Writing test first approach has been adopted throughout this project and the project has `54 unit tests` which covers almost all the business/ application logic. Those tests are written on Model and ViewModel components. 
6. Since Views are dumb and has no logic, no unit tests are written for them. XCUITest could have been used to write UI tests if i had more time.  
7. This project is built on best software engineering practices and code structure is easy to follow. To name a few best practices followed: `SOLID principles, design patterns, loosely coupled architecture and TDD`.

Note: This app has been developed and tested thoroughly and carefully, so nothing should go wrong but Incase such happens please inform recruitement agent so that issue can be addressed.

Many thanks for your time.

